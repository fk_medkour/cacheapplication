package com.test.cachapplication.models;


import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;

public class Cache<K, V> {
    private final Map<K, V> cacheMap;
    private final Map<K, Long> timeMap;
    private final ScheduledExecutorService executorService;
    private final long ttl;

    public Cache(long ttl, TimeUnit timeUnit) {
        this.cacheMap = new HashMap<>();
        this.timeMap = new HashMap<>();
        this.ttl = timeUnit.toMillis(ttl);
        this.executorService = Executors.newScheduledThreadPool(1);

        executorService.scheduleAtFixedRate(this::evictExpiredEntries, this.ttl, this.ttl, TimeUnit.MILLISECONDS);
    }

    public synchronized void put(K key, V value) {
        cacheMap.put(key, value);
        timeMap.put(key, System.currentTimeMillis());
    }

    public synchronized V get(K key) {
        Long insertionTime = timeMap.get(key);
        if (insertionTime == null || System.currentTimeMillis() - insertionTime > ttl) {
            cacheMap.remove(key);
            timeMap.remove(key);
            return null;
        }
        return cacheMap.get(key);
    }

    private synchronized void evictExpiredEntries() {
        long currentTime = System.currentTimeMillis();
        timeMap.keySet().removeIf(key -> currentTime - timeMap.get(key) > ttl);
        cacheMap.keySet().removeIf(key -> !timeMap.containsKey(key));
    }

    public synchronized int size() {
        return cacheMap.size();
    }
}

