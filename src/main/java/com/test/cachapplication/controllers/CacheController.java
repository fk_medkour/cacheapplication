package com.test.cachapplication.controllers;

import com.test.cachapplication.models.Cache;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/api/v1/cache")
public class CacheController {
    private final Cache<String, String> cache = new Cache<>(10, TimeUnit.SECONDS);

    @PostMapping("/put")
    public void put(@RequestParam String key, @RequestParam String value) {
        cache.put(key, value);
    }

    @GetMapping("/get")
    public String get(@RequestParam String key) {
        return cache.get(key);
    }

    @GetMapping("/size")
    public int size() {
        return cache.size();
    }
}
