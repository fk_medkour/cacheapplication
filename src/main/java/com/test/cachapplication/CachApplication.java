package com.test.cachapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CachApplication {

    public static void main(String[] args) {
        SpringApplication.run(CachApplication.class, args);
    }

}
