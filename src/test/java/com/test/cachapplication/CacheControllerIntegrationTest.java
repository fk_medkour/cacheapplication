package com.test.cachapplication;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultHandler;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class CacheControllerIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    private final ResultHandler printResult = result -> {
        System.out.println("Response:");
        System.out.println(result.getResponse().getContentAsString());
    };

    @Test
    public void testPutAndGet() throws Exception {
        mockMvc.perform(post("/api/v1/cache/put")
                        .param("key", "testKey")
                        .param("value", "testValue"))
                .andDo(printResult)
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/v1/cache/get")
                        .param("key", "testKey"))
                .andDo(printResult)
                .andExpect(status().isOk())
                .andExpect(content().string("testValue"));

        Thread.sleep(10000);

        mockMvc.perform(get("/api/v1/cache/get")
                        .param("key", "testKey"))
                .andDo(printResult)
                .andExpect(status().isOk())
                .andExpect(content().string(""));
    }

    @Test
    public void testSize() throws Exception {
        mockMvc.perform(post("/api/v1/cache/put")
                        .param("key", "testKey1")
                        .param("value", "testValue1"))
                .andDo(printResult)
                .andExpect(status().isOk());

        mockMvc.perform(post("/api/v1/cache/put")
                        .param("key", "testKey2")
                        .param("value", "testValue2"))
                .andDo(printResult)
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/v1/cache/size"))
                .andDo(printResult)
                .andExpect(status().isOk())
                .andExpect(content().string("2"));

        Thread.sleep(10000);

        mockMvc.perform(get("/api/v1/cache/size"))
                .andDo(printResult)
                .andExpect(status().isOk())
                .andExpect(content().string("0"));
    }
}
