package com.test.cachapplication;

import com.test.cachapplication.models.Cache;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest
class CachApplicationTests {

    @Test
    public void testCachePutAndGet() throws InterruptedException {
        Cache<String, String> cache = new Cache<>(1, TimeUnit.SECONDS);
        cache.put("key", "value");
        assertEquals("value", cache.get("key"));
        Thread.sleep(2000);
        assertNull(cache.get("key"));
        System.out.println( "testCachePutAndGet : "+ cache.get("key"));
    }

    @Test
    public void testCacheEviction() throws InterruptedException {
        Cache<String, String> cache = new Cache<>(1, TimeUnit.SECONDS);
        cache.put("key1", "value1");
        cache.put("key2", "value2");
        Thread.sleep(2000);
        assertEquals(0, cache.size());
        System.out.println( "testCacheEviction : "+ cache.size());
    }

}
